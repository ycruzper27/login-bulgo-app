import VueRouter from 'vue-router'
import Login from '../views/Login.vue'

const routes = [
  {
    path: '/login',
    name: 'Login Bulgo',
    component: Login,
    literalPathComponent: 'views/Login.vue'
  }
]

if(process.env.VUE_APP_MODE == 'development') {
  routes.unshift({
    path: '/',
    redirect: '/login'
  });
}


const router = new VueRouter({
  mode: 'history',
 	base: process.env.BASE_URL,
  routes
})

export  {
  router,
  routes
}
