import {
    registerFiltersAndHelpersVue,
    ModeApp } from 'bulgo-spa-library'
import Vuetify from '../plugins/vuetify';

export default class Helper extends ModeApp {
    constructor(props) {
        super(props);
        // * Register Filters Vue and helpers $vue
        registerFiltersAndHelpersVue(Vuetify.framework,
            process.env.VUE_APP_MODE == 'development',
            process.env);
    }
    changeConfigDesktop() {
        this.type_app = 'desktop'
        this.changePropertyCss({ variable: '--bg-page', value: 'bg_desktop_login' })
        this.toggleOverlayLoader(false);
    }
    changeConfigDay() {
        this.type_app = 'day_app'
        this.changePropertyCss({ variable: '--bg-page', value: 'bg_day_login' })
        // * Input styles
        this.changePropertyCss({ variable: '--placeholder-color', value: 'blue_static' })
        this.changePropertyCss({ variable: '--color-input-text', value: 'blue_static' })
        this.toggleOverlayLoader(false);
    }
    changeConfigNight() {
        this.type_app = 'night_app'
        this.changePropertyCss({ variable: '--bg-page', value: 'bg_night_login' })
        this.changePropertyCss({ variable: '--chip-outlined-color', value: 'gradient_blue' })
        this.changePropertyCss({ variable: '--chip-color-fill', value: 'gradient_blue' })
        this.changePropertyCss({ variable: '--color-input-text', value: 'blue_static' })
        this.changePropertyCss({ variable: '--placeholder-color', value: 'blue_static' })
        this.toggleOverlayLoader(false);
    }
}