# login-bulgo-app

## Create Module Bulgo App

1. `vue create ${name}-bulgo-app`
2. `vue add vuetify` **Default option**
3. `.env.development` Add: `VUE_APP_MODE=development`
4. `npm i bulgo-spa-library && npm i mobile-device-detect && npm i vue-svg-loader`
5. Modified **package.json** with this:
    ```javascript
        "private":false,
        "main": "dist/${module}.umd.min",
        "build:lib": "npm run bulgo:update && node node_modules/bulgo-spa-library/build/lib.js && vue-cli-service build --target lib --filename ${name}-bulgo-app src/index.js && npm publish",
        "bulgo:update": "npm i bulgo-spa-library@latest",
        "bulgo:commit": "git add . && git commit -m \"${npm_config_message}\" && git push",
        "bulgo:publish": "npm run build:lib && npm run bulgo:commit --message \"${npm_config_msg_publish}\""
    ```

6. Create file `index.js` in **/src** with this:
    ```javascript
        import { routes as routes${name}BulgoApp } from "./router";
        export default { routes${name}BulgoApp, };
    ```
7. Modified `main.js` with this:
    * **Modified** `import { router } from './router'`
    * **Added** `import VueRouter from 'vue-router'`
    * **Added** `Vue.use(VueRouter)`
    * **Added** `import 'bulgo-spa-library/dist/bulgo-spa-library.css';`

8. Modified `index.js` in **/router** with this.
    * **Remove** `import Vue from ‘vue’`
    * **Remove** `Vue.use(VueRouter)`
    * **Added** in `const router`:
        ```javascript
            mode: 'history',
            base: process.env.BASE_URL,
        ```
    * **Added** After create `const router`:
        ```javascript
            routes.unshift({
                path: '/',
                redirect: '/client'
            })
        ```
    * **Modified** `export deafult router`:
        ```javascript
            export  {
                router,
                routes
            }
        ```
9. Copy Folder **/helper** and paste in **/src**.
10. Add in `index.vue` page this:
    ```javascript
        import Helper from './helpers/index'
        created(){
        // * Register Helper
        console.log('register helper', Helper);
        }
    ```
11. **Modified** `vue.config.js` with this:
    * **Added**
        ``` javascript
            css: {
                extract: true,
                loaderOptions: {
                    scss: {
                    additionalData: '@import "~@/assets/scss/global.scss";'
                    }
                }
            },
        ```
    * **Added**
        ``` javascript
            chainWebpack: (config) => {
                const svgRule = config.module.rule('svg');

                svgRule.uses.clear();

                svgRule
                .use('babel-loader')
                .loader('babel-loader')
                .end()
                .use('vue-svg-loader')
                .loader('vue-svg-loader');

                config.devServer.set('inline', false)
                config.devServer.set('hot', true)
            },
        ```