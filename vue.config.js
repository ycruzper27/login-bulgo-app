module.exports = {
  transpileDependencies: [
    'vuetify'
  ],
  css: {
    extract: true,
    loaderOptions: {
        scss: {
          additionalData: `
            @import "@/assets/scss/global.scss";
          `
        }
    }
  },
  chainWebpack: (config) => {
    const svgRule = config.module.rule('svg');

    svgRule.uses.clear();

    svgRule
      .use('babel-loader')
      .loader('babel-loader')
      .end()
      .use('vue-svg-loader')
      .loader('vue-svg-loader');

    config.devServer.set('inline', false)
    config.devServer.set('hot', true)
  },
}
